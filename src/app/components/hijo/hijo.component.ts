import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  // El hijo recibe los mensajes del padre
  @Input() primerMensaje!: string;
  @Input() segundoMensaje!: string;
  @Input() tercerMensaje!: string;
  @Input() cuartoMensaje!: string;
  @Input() quintoMensaje!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
