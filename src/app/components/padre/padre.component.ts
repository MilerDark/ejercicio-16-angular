import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  mensaje1 = 'Wilstermann';
  mensaje2 = 'Real Madrid';
  mensaje3 = 'Champions League';
  mensaje4 = 'Programacion en Java';
  mensaje5 = 'Excel'
  
  constructor() { }

  ngOnInit(): void {
  }

}
